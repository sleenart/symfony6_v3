####

symfony new SymFony-6-V3 --full
symfony console make:docker:database

#####  ajouter les lignes dans docker-compose.yml

phpmyadmin:
    depends_on:
      - database
    image: phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      PMA_HOST: database


######## On lance docker
docker-compose up -d

########  On lance le server
symfony serve -d  
